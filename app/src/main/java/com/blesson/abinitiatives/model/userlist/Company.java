package com.blesson.abinitiatives.model.userlist;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Company implements Serializable {
    private String name;
    private String catchPhrase;
    private String bs;

    @JsonProperty("name")
    public String getName() { return name; }
    @JsonProperty("name")
    public void setName(String value) { this.name = value; }

    @JsonProperty("catchPhrase")
    public String getCatchPhrase() { return catchPhrase; }
    @JsonProperty("catchPhrase")
    public void setCatchPhrase(String value) { this.catchPhrase = value; }

    @JsonProperty("bs")
    public String getBs() { return bs; }
    @JsonProperty("bs")
    public void setBs(String value) { this.bs = value; }
}
