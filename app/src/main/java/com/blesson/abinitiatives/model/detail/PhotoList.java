package com.blesson.abinitiatives.model.detail;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class PhotoList  implements Serializable {
    private long albumId;
    private long id;
    private String title;
    private String url;
    private String thumbnailUrl;

    @JsonProperty("albumId")
    public long getAlbumId() { return albumId; }
    @JsonProperty("albumId")
    public void setAlbumId(long value) { this.albumId = value; }

    @JsonProperty("id")
    public long getId() { return id; }
    @JsonProperty("id")
    public void setId(long value) { this.id = value; }

    @JsonProperty("title")
    public String getTitle() { return title; }
    @JsonProperty("title")
    public void setTitle(String value) { this.title = value; }

    @JsonProperty("url")
    public String getUrl() { return url; }
    @JsonProperty("url")
    public void setUrl(String value) { this.url = value; }

    @JsonProperty("thumbnailUrl")
    public String getThumbnailUrl() { return thumbnailUrl; }
    @JsonProperty("thumbnailUrl")
    public void setThumbnailUrl(String value) { this.thumbnailUrl = value; }
}
