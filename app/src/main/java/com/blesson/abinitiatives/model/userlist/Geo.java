package com.blesson.abinitiatives.model.userlist;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class Geo implements Serializable {
    private String lat;
    private String lng;

    @JsonProperty("lat")
    public String getLat() { return lat; }
    @JsonProperty("lat")
    public void setLat(String value) { this.lat = value; }

    @JsonProperty("lng")
    public String getLng() { return lng; }
    @JsonProperty("lng")
    public void setLng(String value) { this.lng = value; }
}
