package com.blesson.abinitiatives.activity.details;


import com.blesson.abinitiatives.base.BasePresenter;

public class DetailActivityPresenter extends BasePresenter<DetailActivityView> {

    public DetailActivityPresenter(DetailActivityView detailActivityView) {

        super.attachView(detailActivityView);
    }
}
