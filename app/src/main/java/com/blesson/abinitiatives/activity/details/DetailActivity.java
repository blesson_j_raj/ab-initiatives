package com.blesson.abinitiatives.activity.details;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.blesson.abinitiatives.R;
import com.blesson.abinitiatives.base.MvpActivity;
import com.blesson.abinitiatives.fragments.DetailPage.DetailFragment;
import com.blesson.abinitiatives.fragments.Home.HomeFragment;
import com.blesson.abinitiatives.fragments.albamslist.AlbamFragment;
import com.blesson.abinitiatives.model.detail.PhotoList;
import com.blesson.abinitiatives.model.userlist.UserList;
import com.blesson.abinitiatives.utils.ActivityUtils;


public class DetailActivity extends MvpActivity<DetailActivityPresenter> implements DetailActivityView {


    public enum Pages {
        Home,
        DETAIL,
        ALBAM,
    }

    @Override
    protected DetailActivityPresenter createPresenter() {
        return new DetailActivityPresenter(this);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().hide();

        if (getIntent() != null) {
            Pages page = (Pages) getIntent().getSerializableExtra("page");
            loadPage(page);
        }

    }

    private void loadPage(Pages pages) {

        switch (pages) {
            case Home:
                Fragment homeFragment = new HomeFragment();
                ActivityUtils.replaceFragment(getSupportFragmentManager(), homeFragment, R.id.detailsactivity_content);
                break;
            case DETAIL:
                Fragment detailFragment = new DetailFragment();
                PhotoList photoList = (PhotoList) getIntent().getSerializableExtra("userData");
                Bundle detailFragment_Bundle = new Bundle();
                detailFragment_Bundle.putSerializable("userData", photoList);
                detailFragment.setArguments(detailFragment_Bundle);
                ActivityUtils.replaceFragment(getSupportFragmentManager(), detailFragment, R.id.detailsactivity_content);
                break;
            case ALBAM:
                Fragment  albamFragment= new AlbamFragment();
                UserList userData =(UserList) getIntent().getSerializableExtra("userData");
                Bundle albamFragment_buddle = new Bundle();
                albamFragment_buddle.putSerializable("userData", userData);
                albamFragment.setArguments(albamFragment_buddle);
                ActivityUtils.replaceFragment(getSupportFragmentManager(), albamFragment, R.id.detailsactivity_content);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            // handling fragment backbutton navigation
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }

    }
}