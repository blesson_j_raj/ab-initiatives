package com.blesson.abinitiatives.activity.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.blesson.abinitiatives.R;
import com.blesson.abinitiatives.activity.details.DetailActivity;

import java.util.Objects;

public class SplashActivity extends AppCompatActivity {

    Handler handler = null;
    public static final int DELAY_TIME = 3000;
    public static final int SPLASHACTIVITY = 12560;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash);
        Objects.requireNonNull(getSupportActionBar()).hide();
    }


    private void callNextActivity() {
        handler.postDelayed(() -> {
            Intent intent = new Intent(SplashActivity.this, DetailActivity.class);
            intent.putExtra("page", DetailActivity.Pages.Home);
            startActivity(intent);
            finish();


        }, DELAY_TIME);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (handler == null) {
            handler = new Handler();
        }
        callNextActivity();
    }
}
