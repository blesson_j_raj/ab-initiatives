package com.blesson.abinitiatives.network;


import com.blesson.abinitiatives.model.detail.PhotoList;
import com.blesson.abinitiatives.model.userlist.UserList;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

public interface NetworkStores {

    @GET("users")
    Observable<List<UserList>> getUserList();


    @GET("photos")
    Observable<List<PhotoList>> getPhotos();

}