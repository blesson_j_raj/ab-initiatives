package com.blesson.abinitiatives.fragments.DetailPage;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.blesson.abinitiatives.R;
import com.blesson.abinitiatives.base.MvpFragment;
import com.blesson.abinitiatives.model.detail.PhotoList;
import com.squareup.picasso.Picasso;

import java.util.Objects;

public class DetailFragment extends MvpFragment<DetailPresenter> implements DetailVIew {
    private View rootView;
    private ImageView imageView;
    private TextView testViewTitle;

    @Override
    protected DetailPresenter createPresenter() {
        return new DetailPresenter(this);
    }
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return rootView = inflater.inflate(R.layout.fragment_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        onClick();
    }

    private void init() {
        imageView = rootView.findViewById(R.id.imageView);
        testViewTitle = rootView.findViewById(R.id.testViewTitle);

        assert getArguments() != null;
        PhotoList photoList = (PhotoList) getArguments().getSerializable("userData");

     if (photoList != null) {
            testViewTitle.setText(photoList.getTitle());
            Picasso.get().load(photoList.getUrl()).placeholder(R.drawable.placeholder).into(imageView);

        }

    }


    private void onClick() {
        rootView.findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getActivity()).onBackPressed();
            }
        });
    }
}
