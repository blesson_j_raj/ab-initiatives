package com.blesson.abinitiatives.fragments.Home;

import com.blesson.abinitiatives.model.userlist.UserList;

import java.util.List;

public interface HomeView {
    void showLoader();

    void hideLoader();

    void onError(String message);

    void onSetUserList(List<UserList> model);
}
