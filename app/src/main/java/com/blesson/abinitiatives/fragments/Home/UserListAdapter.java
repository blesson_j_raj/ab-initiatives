package com.blesson.abinitiatives.fragments.Home;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;


import com.blesson.abinitiatives.R;
import com.blesson.abinitiatives.model.userlist.UserList;

import java.util.List;

public class UserListAdapter extends RecyclerView.Adapter<UserListAdapter.ViewHolder> {
    private Context context;

    private List<UserList> products;
    private OnClickInterface onClickInterface;

    public UserListAdapter(Context mContext, List<UserList> products, OnClickInterface onClickInterface) {
        this.context = mContext;
        this.products = products;
        this.onClickInterface = onClickInterface;
    }

    public void setProducts(List<UserList> products) {
        this.products = products;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public UserListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(context).inflate(R.layout.custom_user_item_view, parent, false);
        return new UserListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserListAdapter.ViewHolder holder, int position) {

        UserList user = products.get(position);

        holder.textView_usrname.setText(user.getName());
        holder.textView_email.setText(user.getEmail());
        holder.textView_phone.setText(user.getPhone());
        holder.textView_website.setText(user.getWebsite());
        holder.textView_company.setText(user.getCompany().getName());

      holder.itemView.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
           onClickInterface.onCLick(user);
          }
      });
    }

    @Override
    public int getItemCount() {

        if (products != null && products.size() > 0) {
            return products.size();
        } else {
            return 0;
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView textView_usrname;
        private TextView textView_email;
        private TextView textView_phone;
        private TextView textView_website;
        private TextView textView_company;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            textView_usrname = itemView.findViewById(R.id.textView_usrname);
            textView_email = itemView.findViewById(R.id.textView_email);
            textView_phone = itemView.findViewById(R.id.textView_phone);
            textView_website = itemView.findViewById(R.id.textView_website);
            textView_company = itemView.findViewById(R.id.textView_company);

        }
    }

    public interface OnClickInterface {


        void onCLick(UserList userList);

    }
}
