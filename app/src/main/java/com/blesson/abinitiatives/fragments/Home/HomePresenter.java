package com.blesson.abinitiatives.fragments.Home;

import android.content.Context;
import android.widget.Toast;

import com.blesson.abinitiatives.base.BasePresenter;
import com.blesson.abinitiatives.model.userlist.UserList;
import com.blesson.abinitiatives.network.NetworkCallback;

import java.util.List;

public class HomePresenter extends BasePresenter<HomeView> {

    public HomePresenter(HomeView homeView) {
        super.attachView(homeView);
    }

    public void getUserList(Context context) {

        view.showLoader();

        addSubscribe(apiStores.getUserList(), new NetworkCallback<List<UserList>>() {
            @Override
            public void onSuccess(List<UserList> model) {
                view.hideLoader();
                view.onSetUserList(model);


            }
            @Override
            public void onFailure(String message) {
                view.hideLoader();
                view.onError(message);
            }

            @Override
            public void onFinish() {

            }
        });

    }
}
