package com.blesson.abinitiatives.fragments.Home;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.blesson.abinitiatives.R;
import com.blesson.abinitiatives.activity.details.DetailActivity;
import com.blesson.abinitiatives.activity.splash.SplashActivity;
import com.blesson.abinitiatives.base.MvpFragment;
import com.blesson.abinitiatives.model.userlist.UserList;
import com.blesson.abinitiatives.utils.Utils;

import java.util.List;

public class HomeFragment extends MvpFragment<HomePresenter> implements HomeView, UserListAdapter.OnClickInterface {
    private View rootView;
    private RecyclerView recycleView_userlist;
    private LinearLayout linearLayoutLoaderView;

    @Override
    protected HomePresenter createPresenter() {
        return new HomePresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return rootView = inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        onClick();
    }

    private void init() {
        recycleView_userlist = rootView.findViewById(R.id.recycleView_userlist);
        linearLayoutLoaderView = rootView.findViewById(R.id.linearLayoutLoaderView);
        presenter.getUserList(getContext());
    }


    private void onClick() {
    }

    @Override
    public void showLoader() {
        linearLayoutLoaderView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        linearLayoutLoaderView.setVisibility(View.GONE);

    }

    @Override
    public void onError(String message) {
        Utils.infoDialogue(getContext(), message);
    }

    @Override
    public void onSetUserList(List<UserList> model) {
        recycleView_userlist.setAdapter(new UserListAdapter(getContext(), model, this));
    }


    @Override
    public void onCLick(UserList userList) {
        Intent intent = new Intent(getContext(), DetailActivity.class);
        intent.putExtra("page", DetailActivity.Pages.ALBAM);
        intent.putExtra("userData",userList);
        startActivity(intent);
    }
}
