package com.blesson.abinitiatives.fragments.albamslist;

import com.blesson.abinitiatives.model.detail.PhotoList;

import java.util.List;

public interface AlbamVIew {
    void showLoader();

    void hideLoader();

    void onError(String message);

    void setPhotos(List<PhotoList> model);
}
