    package com.blesson.abinitiatives.fragments.albamslist;

    import android.content.Context;
    import android.graphics.drawable.Drawable;
    import android.view.LayoutInflater;
    import android.view.View;
    import android.view.ViewGroup;
    import android.widget.ImageView;

    import androidx.annotation.NonNull;
    import androidx.recyclerview.widget.RecyclerView;

    import com.blesson.abinitiatives.R;
    import com.blesson.abinitiatives.model.detail.PhotoList;
    import com.bumptech.glide.Glide;
    import com.squareup.picasso.Picasso;

    import java.util.List;

    public class PhotoListAdapter extends RecyclerView.Adapter<PhotoListAdapter.ViewHolder> {
        private Context context;

        private List<PhotoList> products;
        private OnClickInterface onClickInterface;

        public PhotoListAdapter(Context mContext, List<PhotoList> products, OnClickInterface onClickInterface) {
            this.context = mContext;
            this.products = products;
            this.onClickInterface = onClickInterface;
        }

        public void setProducts(List<PhotoList> products) {
            this.products = products;
            notifyDataSetChanged();
        }

        @NonNull
        @Override
        public PhotoListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            context = parent.getContext();
            View view = LayoutInflater.from(context).inflate(R.layout.custom_photo_item_view, parent, false);
            return new PhotoListAdapter.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull PhotoListAdapter.ViewHolder holder, int position) {

            PhotoList user = products.get(position);


            Picasso.get().load(user.getUrl()).placeholder(R.drawable.placeholder).into(holder.action_image);

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onClickInterface.onCLick(user);
                }
            });
        }

        @Override
        public int getItemCount() {

            if (products != null && products.size() > 0) {
                return products.size();
            } else {
                return 0;
            }
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView action_image;


            public ViewHolder(@NonNull View itemView) {
                super(itemView);

                action_image = itemView.findViewById(R.id.action_image);


            }
        }

        public interface OnClickInterface {


            void onCLick(PhotoList userList);

        }
    }
