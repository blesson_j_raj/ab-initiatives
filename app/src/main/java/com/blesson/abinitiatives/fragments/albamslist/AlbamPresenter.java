package com.blesson.abinitiatives.fragments.albamslist;

import android.content.Context;
import android.widget.Toast;

import com.blesson.abinitiatives.base.BasePresenter;
import com.blesson.abinitiatives.model.detail.PhotoList;
import com.blesson.abinitiatives.model.userlist.UserList;
import com.blesson.abinitiatives.network.NetworkCallback;

import java.util.List;

public class AlbamPresenter extends BasePresenter<AlbamVIew> {


    public AlbamPresenter(AlbamVIew albamVIew) {
        super.attachView(albamVIew);
    }



    public void getPhotoaList(Context context) {

        view.showLoader();

        addSubscribe(apiStores.getPhotos(), new NetworkCallback<List<PhotoList>>() {
            @Override
            public void onSuccess(List<PhotoList> model) {
                view.hideLoader();
                view.setPhotos(model);


            }
            @Override
            public void onFailure(String message) {
                view.hideLoader();
                view.onError(message);
            }

            @Override
            public void onFinish() {

            }
        });

    }
}
