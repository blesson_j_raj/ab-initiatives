package com.blesson.abinitiatives.fragments.albamslist;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.blesson.abinitiatives.R;
import com.blesson.abinitiatives.activity.details.DetailActivity;
import com.blesson.abinitiatives.base.MvpFragment;
import com.blesson.abinitiatives.model.detail.PhotoList;
import com.blesson.abinitiatives.model.userlist.UserList;
import com.blesson.abinitiatives.utils.Utils;

import java.util.List;
import java.util.Objects;

public class AlbamFragment extends MvpFragment<AlbamPresenter> implements AlbamVIew ,PhotoListAdapter.OnClickInterface{
    private View rootView;
    private TextView testViewTitle;
    private RecyclerView recycleView_photlist;
    private LinearLayout linearLayoutLoaderView;

    @Override
    protected AlbamPresenter createPresenter() {
        return new AlbamPresenter(this);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return rootView = inflater.inflate(R.layout.fragment_albam, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();
        onClick();
    }

    private void init() {
        linearLayoutLoaderView = rootView.findViewById(R.id.linearLayoutLoaderView);
        testViewTitle = rootView.findViewById(R.id.testViewTitle);
        recycleView_photlist = rootView.findViewById(R.id.recycleView_photlist);
        UserList userList = (UserList) getArguments().getSerializable("userData");
        testViewTitle.setText(userList.getName());
        presenter.getPhotoaList(getContext());
    }


    private void onClick() {
        rootView.findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Objects.requireNonNull(getActivity()).onBackPressed();
            }
        });
    }
    @Override
    public void showLoader() {
        linearLayoutLoaderView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoader() {
        linearLayoutLoaderView.setVisibility(View.GONE);

    }

    @Override
    public void onError(String message) {
        Utils.infoDialogue(getContext(), message);
    }


    @Override
    public void setPhotos(List<PhotoList> model) {

        recycleView_photlist.setLayoutManager(new GridLayoutManager(getContext(), 3));

        recycleView_photlist.setAdapter(new PhotoListAdapter(getContext(),model,this));


    }

    @Override
    public void onCLick(PhotoList userList) {

        Intent intent = new Intent(getContext(), DetailActivity.class);
        intent.putExtra("page", DetailActivity.Pages.DETAIL);
        intent.putExtra("userData",userList);
        startActivity(intent);

    }


}
