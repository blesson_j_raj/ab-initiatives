package com.blesson.abinitiatives.base;

import android.content.res.Configuration;
import android.os.Bundle;

import androidx.annotation.Nullable;


import java.util.Locale;
import java.util.Objects;


public abstract class MvpActivity<P extends BasePresenter> extends BaseActivity {
    protected P presenter;

    protected abstract P createPresenter();


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        presenter = createPresenter();
        super.onCreate(savedInstanceState);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.dettachView();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


}